/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FOUNDATION_ACE_FRAMEWORKS_CORE_COMPONENTS_NG_PATTERN_MODEL_MODEL_PAINT_PROPERTY_H
#define FOUNDATION_ACE_FRAMEWORKS_CORE_COMPONENTS_NG_PATTERN_MODEL_MODEL_PAINT_PROPERTY_H

#include "core/components_ng/render/paint_property.h"
#include "core/components_ng/pattern/model/model_property.h"

#include "base/geometry/animatable_float.h"
#include "base/geometry/vec3.h"
#include "foundation/graphic/graphic_3d/3d_widget_adapter/include/custom/custom_render_descriptor.h"
#include "foundation/graphic/graphic_3d/3d_widget_adapter/include/data_type/geometry/geometry.h"
#include "foundation/graphic/graphic_3d/3d_widget_adapter/include/data_type/gltf_animation.h"
#include "foundation/graphic/graphic_3d/3d_widget_adapter/include/data_type/light.h"
#include "foundation/graphic/graphic_3d/3d_widget_adapter/include/data_type/shader_input_buffer.h"

namespace OHOS::Ace::NG {

class ModelPaintProperty : public PaintProperty {
    DECLARE_ACE_TYPE(ModelPaintProperty, PaintProperty)

public:
    ModelPaintProperty() {
        // Initialize model lights to empty vector;
        propModelLights_ = std::vector<RefPtr<OHOS::Render3D::SVLight>> {};
        propModelAnimations_ = std::vector<RefPtr<OHOS::Render3D::GLTFAnimation>> {};
        propModelGeometries_ = std::vector<RefPtr<OHOS::Render3D::SVGeometry>> {};
        propModelCustomRenders_ = std::vector<RefPtr<OHOS::Render3D::SVCustomRenderDescriptor>> {};
        propModelImageTexturePaths_ = std::vector<std::string> {};
        propModelShaderInputBuffers_ = std::vector<RefPtr<OHOS::Render3D::ShaderInputBuffer>> {};
    };
    ~ModelPaintProperty() override = default;

    RefPtr<PaintProperty> Clone() const override
    {
        auto paintProperty = MakeRefPtr<ModelPaintProperty>();
        paintProperty->UpdatePaintProperty(this);
        paintProperty->propCameraPosition_ = CloneCameraPosition();
        paintProperty->propCameraDistance_ = CloneCameraDistance();
        paintProperty->propCameraIsAngular_ = CloneCameraIsAngular();
        paintProperty->propCameraRotation_ = CloneCameraRotation();
        paintProperty->propCameraLookAt_ = CloneCameraLookAt();
        paintProperty->propCameraUp_ = CloneCameraUp();
        paintProperty->propCameraZNear_ = CloneCameraZNear();
        paintProperty->propCameraZFar_ = CloneCameraZFar();
        paintProperty->propCameraFOV_ = CloneCameraFOV();
        paintProperty->propModelLights_ = CloneModelLights();
        paintProperty->propModelAnimations_ = CloneModelAnimations();
        paintProperty->propModelGeometries_ = CloneModelGeometries();
        paintProperty->propModelCustomRenders_ = CloneModelCustomRenders();
        paintProperty->propShaderPath_ = CloneShaderPath();
        paintProperty->propModelImageTexturePaths_ = CloneModelImageTexturePaths();
        paintProperty->propModelShaderInputBuffers_ = CloneModelShaderInputBuffers();

        paintProperty->needsCameraSetup_ = CloneNeedsCameraSetup();
        paintProperty->needsLightsSetup_ = CloneNeedsLightsSetup();
        paintProperty->needsAnimationsSetup_ = CloneNeedsAnimationsSetup();
        paintProperty->needsGeometriesSetup_ = CloneNeedsGeometriesSetup();
        paintProperty->needsCustomRendersSetup_ = CloneNeedsCustomRendersSetup();
        paintProperty->needsShaderPathSetup_ = CloneNeedsShaderPathSetup();
        paintProperty->needsImageTexturePathsSetup_ = CloneNeedsImageTexturePathsSetup();
        paintProperty->needsShaderInputBuffersSetup_ = CloneNeedsShaderInputBuffersSetup();
        return paintProperty;
    }

    void ResetFlagProperties()
    {
        UpdateNeedsAnimationsSetup(false);
        UpdateNeedsGeometriesSetup(false);
        UpdateNeedsCameraSetup(false);
        UpdateNeedsLightsSetup(false);
        UpdateNeedsCustomRendersSetup(false);
        UpdateNeedsShaderPathSetup(false);
        UpdateNeedsImageTexturePathsSetup(false);
        UpdateNeedsShaderInputBuffersSetup(false);
    }

    void Reset() override
    {
        PaintProperty::Reset();
        ResetCameraPosition();
        ResetCameraDistance();
        ResetCameraIsAngular();
        ResetCameraRotation();
        ResetCameraLookAt();
        ResetCameraUp();
        ResetCameraZNear();
        ResetCameraZFar();
        ResetCameraFOV();
        ResetModelLights();
        ResetModelSingleLight();
        ResetModelAnimations();
        ResetModelSingleAnimation();
        ResetModelGeometries();
        ResetModelSingleGeometry();
        ResetModelCustomRenders();
        ResetModelSingleCustomRender();
        ResetShaderPath();
        ResetModelImageTexturePaths();
        ResetModelSingleImageTexturePath();
        ResetModelShaderInputBuffers();
        ResetModelSingleShaderInputBuffer();
        ResetFlagProperties();
    }

    void ModelLightsAnimationUpdate(const std::vector<RefPtr<OHOS::Render3D::SVLight>>& lights)
    {
        propModelLights_ = lights;
        UpdatePropertyChangeFlag(PROPERTY_UPDATE_RENDER);
        UpdateNeedsLightsSetup(true);
    }

    void OnModelSingleLightUpdate(const RefPtr<OHOS::Render3D::SVLight>& light)
    {
        propModelLights_.value().push_back(light);
        LOGD("MODEL_NG: propModelLights_.size() -> %zu", GetModelLightsValue().size());
        ResetModelSingleLight();
        UpdateNeedsLightsSetup(true);
    }

    void OnModelSingleAnimationUpdate(const RefPtr<OHOS::Render3D::GLTFAnimation>& animation)
    {
        propModelAnimations_.value().push_back(animation);
        LOGD("MODEL_NG: propModelAnimations_.size() -> %zu", GetModelAnimationsValue().size());
        ResetModelSingleAnimation();
        UpdateNeedsAnimationsSetup(true);
    }

    void OnModelSingleGeometryUpdate(const RefPtr<OHOS::Render3D::SVGeometry>& geometry)
    {
        propModelGeometries_.value().push_back(geometry);
        LOGD("MODEL_NG: propModelGeometries_.size() -> %zu", GetModelGeometriesValue().size());
        ResetModelSingleGeometry();
        UpdateNeedsGeometriesSetup(true);
    }

    void OnModelSingleCustomRenderUpdate(const RefPtr<OHOS::Render3D::SVCustomRenderDescriptor>& customRender)
    {
        propModelCustomRenders_.value().push_back(customRender);
        LOGD("MODEL_NG: propModelCustomRenders_.size() -> %zu", GetModelCustomRendersValue().size());
        ResetModelSingleCustomRender();
        UpdateNeedsCustomRendersSetup(true);
    }

    void OnModelSingleImageTexturePathUpdate(const std::string& path)
    {
        propModelImageTexturePaths_.value().push_back(path);
        LOGD("MODEL_NG: propModelImageTexturePaths_: %zu", GetModelImageTexturePathsValue().size());
        ResetModelSingleImageTexturePath();
        UpdateNeedsImageTexturePathsSetup(true);
    }

    void OnModelSingleShaderInputBufferUpdate(const RefPtr<OHOS::Render3D::ShaderInputBuffer>& buffer)
    {
        propModelShaderInputBuffers_.value().push_back(buffer);
        LOGD("MODEL_NG: propModelShaderInputBuffers_: %zu", GetModelShaderInputBuffersValue().size());
        ResetModelSingleShaderInputBuffer();
        UpdateNeedsShaderInputBuffersSetup(true);
    }

    DEFINE_NEEDS_SETUP_FLAG_TRIGGER_PROPERTY(
        CameraPosition, OHOS::Ace::Vec3, Camera, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_TRIGGER_PROPERTY(
        CameraDistance, OHOS::Ace::AnimatableFloat, Camera, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_TRIGGER_PROPERTY(
        CameraIsAngular, bool, Camera, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_TRIGGER_PROPERTY(
        CameraRotation, OHOS::Ace::Quaternion, Camera, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_TRIGGER_PROPERTY(
        CameraLookAt, OHOS::Ace::Vec3, Camera, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_TRIGGER_PROPERTY(
        CameraUp, OHOS::Ace::Vec3, Camera, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_TRIGGER_PROPERTY(
        CameraZNear, float, Camera, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_TRIGGER_PROPERTY(
        CameraZFar, float, Camera, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_TRIGGER_PROPERTY(
        CameraFOV, float, Camera, PROPERTY_UPDATE_RENDER);

    DEFINE_NEEDS_SETUP_FLAG_TRIGGER_PROPERTY(
        ShaderPath, std::string, ShaderPath, PROPERTY_UPDATE_RENDER);

    ACE_DEFINE_PROPERTY_ITEM_WITHOUT_GROUP(
        ModelLights, std::vector<RefPtr<OHOS::Render3D::SVLight>>, PROPERTY_UPDATE_RENDER);
    ACE_DEFINE_PROPERTY_ITEM_WITHOUT_GROUP_AND_USING_CALLBACK(
        ModelSingleLight, RefPtr<OHOS::Render3D::SVLight>, PROPERTY_UPDATE_RENDER);

    ACE_DEFINE_PROPERTY_ITEM_WITHOUT_GROUP(
        ModelAnimations, std::vector<RefPtr<OHOS::Render3D::GLTFAnimation>>, PROPERTY_UPDATE_RENDER);
    ACE_DEFINE_PROPERTY_ITEM_WITHOUT_GROUP_AND_USING_CALLBACK(
        ModelSingleAnimation, RefPtr<OHOS::Render3D::GLTFAnimation>, PROPERTY_UPDATE_RENDER);

    ACE_DEFINE_PROPERTY_ITEM_WITHOUT_GROUP(
        ModelGeometries, std::vector<RefPtr<OHOS::Render3D::SVGeometry>>, PROPERTY_UPDATE_RENDER);
    ACE_DEFINE_PROPERTY_ITEM_WITHOUT_GROUP_AND_USING_CALLBACK(
        ModelSingleGeometry, RefPtr<OHOS::Render3D::SVGeometry>, PROPERTY_UPDATE_RENDER);

    ACE_DEFINE_PROPERTY_ITEM_WITHOUT_GROUP(
        ModelCustomRenders, std::vector<RefPtr<OHOS::Render3D::SVCustomRenderDescriptor>>, PROPERTY_UPDATE_RENDER);
    ACE_DEFINE_PROPERTY_ITEM_WITHOUT_GROUP_AND_USING_CALLBACK(
        ModelSingleCustomRender, RefPtr<OHOS::Render3D::SVCustomRenderDescriptor>, PROPERTY_UPDATE_RENDER);

    ACE_DEFINE_PROPERTY_ITEM_WITHOUT_GROUP(
        ModelImageTexturePaths, std::vector<std::string>, PROPERTY_UPDATE_RENDER);
    ACE_DEFINE_PROPERTY_ITEM_WITHOUT_GROUP_AND_USING_CALLBACK(
        ModelSingleImageTexturePath, std::string, PROPERTY_UPDATE_RENDER);

    ACE_DEFINE_PROPERTY_ITEM_WITHOUT_GROUP(
        ModelShaderInputBuffers, std::vector<RefPtr<OHOS::Render3D::ShaderInputBuffer>>, PROPERTY_UPDATE_RENDER);
    ACE_DEFINE_PROPERTY_ITEM_WITHOUT_GROUP_AND_USING_CALLBACK(
        ModelSingleShaderInputBuffer, RefPtr<OHOS::Render3D::ShaderInputBuffer>, PROPERTY_UPDATE_RENDER);

    DEFINE_NEEDS_SETUP_FLAG_PROPERTY(Camera, false, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_PROPERTY(Lights, false, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_PROPERTY(Animations, false, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_PROPERTY(Geometries, false, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_PROPERTY(CustomRenders, false, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_PROPERTY(ShaderPath, false, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_PROPERTY(ImageTexturePaths, false, PROPERTY_UPDATE_RENDER);
    DEFINE_NEEDS_SETUP_FLAG_PROPERTY(ShaderInputBuffers, false, PROPERTY_UPDATE_RENDER);

private:
    ACE_DISALLOW_COPY_AND_MOVE(ModelPaintProperty);
};

} // namespace OHOS::Ace::NG

#endif // FOUNDATION_ACE_FRAMEWORKS_CORE_COMPONENTS_NG_PATTERN_MODEL_MODEL_PAINT_PROPERTY_H
